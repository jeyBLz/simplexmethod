﻿using System;
using System.Collections.Generic;

namespace SimplexMethod
{
    public class Simplex
    {
        Table table;

        public Simplex(double[,] source)
        {

            table = new Table(source.GetLength(0), source.GetLength(1));

            for (int i = 0; i < table.GetHeight(); i++)
            {
                for (int j = 0; j < table.GetTable().GetLength(1); j++)
                    table[i, j] = (j < table.GetWidth()) ? source[i, j] : 0;

                if ((table.GetWidth() + i) < table.GetTable().GetLength(1))
                {
                    table[i, table.GetWidth() + i] = 1;
                    table.AddToBasis(table.GetWidth() + i);
                }
            }

            table.SetWidth(table.GetTable().GetLength(1));
        }

        public Simplex DoCalculations(double[] result)
        {
            while (!IsFinished())
            {

                int mainCol = FindMainCol();
                int mainRow = FindMainRow(mainCol);

                printTable(mainCol, mainRow);

                table.SetBasis(mainRow, mainCol);

                double[,] newTable = new double[table.GetHeight(), table.GetWidth()];

                for (int j = 0; j < table.GetWidth(); j++)
                    newTable[mainRow, j] = table[mainRow, j] / table[mainRow, mainCol];

                for (int i = 0; i < table.GetHeight(); i++)
                {
                    if (i.Equals(mainRow))
                        continue;

                    for (int j = 0; j < table.GetWidth(); j++)
                        newTable[i, j] = table[i, j] - table[i, mainCol] * newTable[mainRow, j];
                }
                table.SetTable(newTable);
            }

            for (int i = 0; i < result.Length; i++)
            {
                int k = table.GetBasis().IndexOf(i + 1);
                result[i] = (k != -1) ? table[k, 0] : 0;
            }

            printTable();

            return this;
        }

        public void printTable()
        {
            Console.WriteLine("Вывод таблицы:\n");
            for (int i = 0; i < table.GetTable().GetLength(0); i++)
            {
                for (int j = 1; j < table.GetTable().GetLength(1); j++)
                {
                    Console.Write(String.Format("|{0,5}|", Math.Round(table[i, j], 2)));
                }
                Console.Write(String.Format("|{0,5}|", Math.Round(table[i, 0], 2)));
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        public void printTable(int mainCol, int mainRow)
        {
            Console.WriteLine("Вывод таблицы:\n");
            for (int i = 0; i < table.GetTable().GetLength(0); i++)
            {
                String format;
                for (int j = 1; j < table.GetTable().GetLength(1); j++)
                {
                    if (j.Equals(mainCol) || i.Equals(mainRow))
                        format = "({0,5})";
                    else
                        format = "|{0,5}|";
                    Console.Write(String.Format(format, Math.Round(table[i, j], 2)));
                }
                if (i.Equals(mainRow))
                    format = "({0,5})";
                else
                    format = "|{0,5}|";
                Console.Write(String.Format(format, Math.Round(table[i, 0], 2)));
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        private int FindMainCol()
        {
            int mainCol = 1;

            for (int j = 2; j < table.GetWidth(); j++)
                if (table[table.GetHeight() - 1, j].CompareTo(table[table.GetHeight() - 1, mainCol]) < 0)
                    mainCol = j;

            return mainCol;
        }

        private int FindMainRow(int mainCol)
        {
            int mainRow = 0;

            for (int i = 0; i < table.GetHeight() - 1; i++)
                if (table[i, mainCol] > 0)
                {
                    mainRow = i;
                    break;
                }

            for (int i = mainRow + 1; i < table.GetHeight() - 1; i++)
                if ((table[i, mainCol] > 0) && ((table[i, 0] / table[i, mainCol]).CompareTo(table[mainRow, 0] / table[mainRow, mainCol]) < 0))
                    mainRow = i;

            return mainRow;
        }

        private bool IsFinished()
        {
            for (int j = 1; j < table.GetWidth(); j++)
            {
                if (table[table.GetHeight() - 1, j] < 0)
                {
                    return false;
                }
            }

            return true;
        }

    }
}