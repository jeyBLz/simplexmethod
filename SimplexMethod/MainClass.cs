﻿using SimplexMethod;
using System;

namespace testSimplex
{
    class MainClass
    {

        static void Main(string[] args)
        {
            // Первый столбец - вектор b, остальные как всегда

            double[,] testTable1 = { 
                { 2, -1,  1},
                {14,  3,  2},
                {17,  4,  1},
                { 0, -3, -2} 
            };

            double[,] testTable2 = {
                { 24, -1, 3},
                { 48, 1, 5},
                { 8, 4, -3},
                { 0, -5, -1}
            };

            double[,] testTable3 = {
                { 12, 1, 3},
                { 12, 3, 1},
                { 0, -1, -3}
            };

            double[] result = new double[2];
            Simplex S = new Simplex(testTable2);
            S.DoCalculations(result);

            Console.WriteLine(String.Format("Результат: ({0,1}, {1,1})", result[0], result[1]));
        }
    }
}