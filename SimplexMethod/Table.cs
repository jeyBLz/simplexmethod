﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimplexMethod
{
    class Table
    {
        double[,] table;
        List<int> basis;
        int height, width;

        public Table(int height, int width) 
        {
            this.table = new double[height, width + height - 1];
            this.basis = new List<int>();
            this.height = height;
            this.width = width;
        }

        public double this[int i, int j]
        {
            get { return table[i, j]; }
            set { table[i, j] = value; }
        }

        public void AddToBasis(int num)
        {
            basis.Add(num);
        }

        public List<int> GetBasis()
        {
            return basis;
        }

        public void SetBasis(int index, int num)
        {
            basis[index] = num;
        }

        public double[,] GetTable()
        {
            return table;
        }

        public void SetTable(double[,] table)
        {
            this.table = table;
        }

        public int GetHeight()
        {
            return height;
        }

        public int GetWidth()
        {
            return width;
        }

        public void SetHeight(int height)
        {
            this.height = height;
        }

        public void SetWidth(int width)
        {
            this.width = width;
        }
    }
}
